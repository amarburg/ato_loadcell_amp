#!/usr/bin/env python3

import minimalmodbus
import serial

instrument = minimalmodbus.Instrument('/dev/ttyUSB1', 1, debug=True)  # port name, address
instrument.serial.baudrate = 9600

## Read number of decimals
dec = instrument.read_long(0x06)
scalar = 10**dec
print("%d decimal places, scalar %f" % (dec,scalar))

## Read the raw force
rawval = instrument.read_long(0x00, signed=True)  
print(rawval)

## TBD, the exact conversion from the read value to N needs to be tested.
kg = rawval/scalar
n = kg * 9.81 

print("Weight : %f kg.  Force : %f N" % (kg,n))

