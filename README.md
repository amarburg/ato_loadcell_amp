 Early experiments in reading data from the [ATO Digital Load Cell Transmitter](https://www.ato.com/digital-load-cell-amplifier-output-4-20ma-0-10v-rs485) over RS485.

Contains a python script `scripts/read_force.py` and a simple ROS node `ato_loadcell_driver` 

# ROS node

To run the ROS node, check this repository into the `src/` directory of your Catkin workscape, then `catkin_make` or `catkin build`

Then

```
source devel/setup.bash
rosrun ato_loadcell_amp ato_loadcell_driver _port:=/dev/ttyUSB0
```
