from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['ato_loadcell_amp'],
#    scripts=['bin/sonar_roll'],
#    package_dir={'': 'lib'}
)

setup(**d)
